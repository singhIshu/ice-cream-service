package iceCreamService.repository;

import iceCreamService.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Repository
public class RoleRepository implements iceCreamService.repository.Repository<Role> {
    private AirTableClient airTableClient;

    @Autowired
    public RoleRepository(AirTableClient airTableClient) {
        this.airTableClient = airTableClient;
    }

    @Override
    public void save(Role roleRecord) {
        airTableClient.save("Role", roleRecord);
    }

    @Override
    public List<Role> findByField(Predicate<Role> rolePredicate) {
        return airTableClient
                .findAll("Role", Role.class)
                .stream().filter(rolePredicate)
                .collect(Collectors.toList());
    }

    @Override
    public List<Role> findAll() {
        return airTableClient.findAll("Role", Role.class);
    }

    @Override
    public List<Role> findById(String userId) {
        return null;
    }
}
