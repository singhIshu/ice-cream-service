package iceCreamService.repository.response;


import java.util.List;
import java.util.Map;

public class AirTableUserResponse {
    private List<Map<String, Object>> records;

    public AirTableUserResponse() {
    }

    public List<Map<String, Object>> getRecords() {
        return records;
    }

    public void setRecords(List<Map<String, Object>> records) {
        this.records = records;
    }

}