package iceCreamService.repository;

import iceCreamService.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class UserRepository  implements Repository<User>{

    private AirTableClient airTableClient;
    private String TABLE_NAME = "User";

    @Autowired
    public UserRepository(AirTableClient airTableClient) {
        this.airTableClient = airTableClient;
    }

    @Override
    public List<User> findAll() {
        return airTableClient.findAll(TABLE_NAME, User.class);
    }

    @Override
    public List<User> findById(String userId) {
        return findAll().stream().filter(it -> it.getName().equals(userId)).collect(Collectors.toList());
    }

    @Override
    public void save(User user) {
        airTableClient.save(TABLE_NAME, user);
    }

    @Override
    public List<User> findByField(Predicate<User> email) {
        return findAll();
    }
}
