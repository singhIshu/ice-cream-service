package iceCreamService.repository;
import java.util.List;
import java.util.function.Predicate;

public interface Repository <T> {
    List<T> findAll();

    public List<T> findById(String userId);

    public void save(T t);

    public List<T> findByField(Predicate<T> predicate);
}
