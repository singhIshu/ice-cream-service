package iceCreamService.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import iceCreamService.repository.response.AirTableUserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

@Component
public class AirTableClient {

    private RestTemplate restTemplate;
    private String uri = "https://api.airtable.com/v0/appiZcFXI4ZuQQsrT/";
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    public AirTableClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    <T> List<T> findAll(String tableName, Class<T> t) {

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(singletonList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer keyvYYo0Vrc222PGV");
        HttpEntity<String> entity = new HttpEntity("parameters", headers);

        return Objects.requireNonNull(restTemplate.exchange(
                uri + tableName,
                HttpMethod.GET,
                entity,
                AirTableUserResponse.class)
                .getBody())
                .getRecords()
                .stream()
                .map(it -> {
                    HashMap hashMap = new HashMap();
                    hashMap.putAll((Map) it.get("fields"));
                    hashMap.put("id", it.get("id"));
                    return mapper.convertValue(hashMap, t);
                })
                .collect(Collectors.toList());
    }

    class ALB{


    }
    public <T> void save(String tableName, T t) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "Bearer keyvYYo0Vrc222PGV");
        Map<String, String> values = mapper.convertValue(t, new TypeReference<Map<String, String>>() {});
        Map<String, Object> requestBody = new HashMap<>();

        requestBody.put("fields", values);
        requestBody.put("typecast", true);

        HttpEntity<Map> request = new HttpEntity(requestBody, headers);

        restTemplate.postForEntity(URI.create(uri + tableName),
                request, Object.class);
    }
}
