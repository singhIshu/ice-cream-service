package iceCreamService.repository;

import iceCreamService.model.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


@Repository
public class MemberRepository {
    private AirTableClient airTableClient;

    @Autowired
    public MemberRepository(AirTableClient airTableClient) {
        this.airTableClient = airTableClient;
    }

    public void save(Member member) {
        airTableClient.save("Member", member);
    }

    public List<Member> findByField(Predicate<Member> rolePredicate) {
        return airTableClient
                .findAll("Member", Member.class)
                .stream().filter(rolePredicate)
                .collect(Collectors.toList());
    }

    public List<Member> findAll() {
        return airTableClient.findAll("Member", Member.class);
    }

    public boolean existsById(String memberId) {
        return findByField(it -> it.getId().equals(memberId)).isEmpty();
    }
}


