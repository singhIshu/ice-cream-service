package iceCreamService.dto;

import iceCreamService.model.User;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserDto {

    public static Map<String, Object> toRequest(User user) {
        HashMap userInfo = new HashMap<String, String>();
        userInfo.put("email", user.getEmail());
        userInfo.put("name", user.getName());
        userInfo.put("password", user.getPassword());
        Map<String, Object> updateRequest = new HashMap<>();
        updateRequest.put("fields", userInfo);
        updateRequest.put("typecast", true);
        return updateRequest;

    }

    public static User fromResponse(Map<String, Object> response) {
        User user = new User();
        Map<String, String> fields = (Map<String, String>) response.get("fields");
        user.setName(fields.get("name"));
        user.setName(fields.get("email"));
        user.setPassword(fields.get("password"));
        return user;
    }
}
