package iceCreamService.controller;

import iceCreamService.exception.UserNotFoundException;
import iceCreamService.model.User;
import iceCreamService.request.NewUserRequest;
import iceCreamService.service.SessionService;
import iceCreamService.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;


@RestController
public class UserController {

    private UserService userService;
    private SessionService sessionService;

    @Autowired
    public UserController(UserService userService, SessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    @GetMapping("/allUsers")
    public List<User> getAllTheUsers() {
        return userService.getAllUsers();
    }

    @CrossOrigin
    @PostMapping("/addUser")
    ResponseEntity addNewUser(@RequestBody NewUserRequest newUserRequest) {
        userService.addUser(newUserRequest.name, newUserRequest.email, newUserRequest.password);
        String token = UUID.randomUUID().toString() + ":" + System.currentTimeMillis();
        sessionService.addSession(token, newUserRequest.email);
        return new ResponseEntity(token, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("/user/{email}")
    public User getUserByEmail(@PathVariable String email) throws UserNotFoundException {
        return userService.getUserByEmail(email);
    }
}
