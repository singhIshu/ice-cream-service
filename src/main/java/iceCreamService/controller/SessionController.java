package iceCreamService.controller;

import iceCreamService.request.LoginTeam;
import iceCreamService.service.SessionService;
import iceCreamService.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
public class SessionController {

    private SessionService sessionService;
    private UserService userService;

    public SessionController(SessionService sessionService, UserService userService) {
        this.sessionService = sessionService;
        this.userService = userService;
    }

    @CrossOrigin
    @PostMapping("/loginUser")
    public ResponseEntity loginUser(@RequestBody LoginTeam loginTeam) {
        if (userService.isValidEmailAndPassword(loginTeam.email, loginTeam.password)) {
            String token = UUID.randomUUID().toString() + ":" + System.currentTimeMillis();
            sessionService.addSession(token, loginTeam.email);
            return new ResponseEntity(token, HttpStatus.OK);
        } else {
            return new ResponseEntity<String>(HttpStatus.UNAUTHORIZED);
        }
    }

    @CrossOrigin
    @PostMapping("/signOutUser")
    public ResponseEntity signOutUser(
            @RequestHeader(value = "email") String email,
            @RequestHeader(value = "accessToken") String accessToken) {
        if (sessionService.isValidSession(accessToken, email)) {
            sessionService.removeSession(accessToken);
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    @CrossOrigin
    @GetMapping("/isLoggedIn")
    public ResponseEntity isLoggedIn(
            @RequestHeader(value = "email") String email,
            @RequestHeader(value = "accessToken") String accessToken) {
        if (sessionService.isValidSession(accessToken, email)) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity(HttpStatus.UNAUTHORIZED);
    }
}
